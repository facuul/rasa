from rasa_nlu.model import Interpreter
import json
from flask import Flask, request
from pymessenger.bot import Bot
import requests

app = Flask(__name__)
interpreter = Interpreter.load("./models/current/nlu")

ACCESS_TOKEN = 'EAAFjgBKMzFkBAM5nyI4SnbLyHQyr1p0YEcC8CjOZCZCHA3iDoLUZApfoijBqmDHcQkERwjEdRZApkjGTgodIIpeNTHeC2UUSBNfggLSAuEYVirW7iArfZBboIfjSIoNIAeW9QRC4IYW62utccCjQDx2sPJZAw0HLZBHiG67AgdzTbjOdZBZAypGQA'
VERIFY_TOKEN = 'verify'
URL = 'http://data.fixer.io/api/latest?access_key=fb4f1bb52aa744e9ac9cdf42194bc2bd'


bot = Bot(ACCESS_TOKEN)

def get_intent(response):
    if response.get('intent'):
        return response['intent']['name']

    return ''


def get_entity(response, entity):
    if response.get('entities'):
        entity_value = [i['value'] for i in response['entities'] if i['entity'] == entity]
        if len(entity_value) > 0:
            return entity_value[0]

    return ''


def convert(response):
    amount = get_entity(response, 'amount')
    to_ = get_entity(response,'to').upper()
    from_ = get_entity(response, 'from').upper()
    print(URL+'&symbols='+from_+','+to_)
    url_response = requests.get(URL+'&symbols='+from_+','+to_)
    print(url_response.text)
    rates = json.loads(url_response.text)['rates']
    print(rates)

    if float(rates[from_]) != 0 and to_ in rates and from_ in rates:
        result = float(rates[to_]) * float(amount) / float(rates[from_])
        return round(result,2)

    return 'Can not convert, please try something else!'

@app.route('/', methods=['GET', 'POST'])
def webhook():
    if request.method == 'GET':
        if request.args.get('hub.verify_token') == VERIFY_TOKEN:
            return request.args.get('hub.challenge')
    else:
        data = request.get_json()
        if data['object'] == 'page':
            for event in data['entry']:
                messaging = event['messaging']
                for message in messaging:
                    recipient_id = message['sender']['id']
                    bot.send_text_message(recipient_id, 'já respondo...')
                    if message['message'].get('text'):
                        text = message['message']['text']
                        response = interpreter.parse(text)

                        print (response)

                        if get_intent(response) == 'greet':
                            bot.send_text_message(recipient_id, text)

                        if get_intent(response) == 'convert':
                            bot.send_text_message(recipient_id, convert(response))


    return 'invalid token'

app.run(port=5000, debug=True)
